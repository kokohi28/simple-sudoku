package com.divt.simplesudoku;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class GameActivity extends AppCompatActivity
    implements
    CellGroup.OnFragmentInteractionListener {
  private static final String TAG = GameActivity.class.getSimpleName();

  private TextView mTvClickedCell;
  private int mClickedGroup;
  private int mClickedCellId;

  private Board mStartBoard;
  private Board mCurrentBoard;

  private TextView mTvStatus;

  public static final int RC_FILE = 102;

  private boolean mHasReadStoragePermission = false;
  private RelativeLayout mRlPermission = null;
  private LinearLayout mLlSetPermission = null;

  private static Handler mHandler = new Handler();

  public static Handler getHandler() {
    return mHandler;
  }

  private static final int PATTERN_CLEAR = -1;
  private static final int PATTERN_EASY = 0;
  private static final int PATTERN_NORMAL = 1;
  private static final int PATTERN_HARD = 2;
  private static final int PATTERN_OTHER = 99;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_game);

    // Init permission UI
    mRlPermission = findViewById(R.id.rl_permission);
    mLlSetPermission = findViewById(R.id.ll_set_permission);

    mTvStatus = findViewById(R.id.tv_status);
    Button buttonSolve = findViewById(R.id.btn_solve);
    buttonSolve.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        trySolve();
      }
    });

    if (!checkNSetViewPermission()) return;
    startApp();
  }

  private void startApp() {
    getHandler().post(new Runnable() {
      @Override
      public void run() {
        mHasReadStoragePermission = true;
        prepareBoard(PATTERN_EASY, null);
        invalidateOptionsMenu();
      }
    });
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, int[] grantResults) {
    Log.d(TAG, "-- onRequestPermissionsResult() : " + requestCode);

    Log.d(TAG, "... Perms : NOT HANDLED or UNKNOWN REQUEST CODE");
    if (requestCode == MPermsManager.REQUEST_WITH_SPECIFIC_PURPOSE) {
      // MANDATORY Permission
      String[] mandatoryPerms = new String[]{
          MPermsManager.P_READ_EXTERNAL_STORAGE
      };

      if (MPermsManager.verifyWithSpecificPermissions(grantResults, permissions, mandatoryPerms)) {
        Log.d(TAG, "... Perms mandatory : GRANTED");
        if (mRlPermission.getVisibility() != View.GONE) mRlPermission.setVisibility(View.GONE);
        startApp();
      } else {
        Log.d(TAG, "... Perms mandatory : DENY");
        // TODO Tampilkan dialog menuju app setting
      }
    } else if (requestCode == MPermsManager.REQUEST_ALL_OPTIONAL) {
      if (MPermsManager.verifyPermissions(grantResults)) {
        Log.d(TAG, "... Perms opt : GRANTED");
      } else {
        Log.d(TAG, "... Perms opt : DENY");
      }
    } else {
      Log.d(TAG, "... Perms : UNKNOWN REQUEST CODE");
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }

  private boolean checkNSetViewPermission() {
    // Koko, check permission MPermsManager.P_READ_EXTERNAL_STORAGE // MANDATORY

    // MANDATORY Permission
    String[] mandatoryPerms = new String[]{
        MPermsManager.P_READ_EXTERNAL_STORAGE
    };

    boolean permission_pass;
    if (!MPermsManager.isAllPermsGranted(this, mandatoryPerms)) {
      Log.e(TAG, "!! We don't have mandatory permission");
      permission_pass = false;
      if (mRlPermission.getVisibility() != View.VISIBLE) {
        mRlPermission.setVisibility(View.VISIBLE);
      }

      final String[] extraPerms = new String[]{
          MPermsManager.P_READ_EXTERNAL_STORAGE
      };

      mLlSetPermission.setOnClickListener(null);
      mLlSetPermission.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          MPermsManager.requestPerms(GameActivity.this, extraPerms, MPermsManager.REQUEST_WITH_SPECIFIC_PURPOSE);
        }
      });
    } else {
      permission_pass = true;
      if (mRlPermission.getVisibility() != View.GONE) mRlPermission.setVisibility(View.GONE);
    }

    return permission_pass;
  }

  private void prepareBoard(int difficulty, String pathFile) {
    mTvStatus.setVisibility(View.GONE);

    ArrayList<Board> boards = readGameBoards(difficulty, pathFile);
    mStartBoard = null;
    mCurrentBoard = null;

    mCurrentBoard = new Board();
    mStartBoard = chooseRandomBoard(boards);
    mCurrentBoard.copyValues(mStartBoard.getCells());

    int[] cellGroupFragments = new int[] {
        R.id.cellGroupFragment,
        R.id.cellGroupFragment2,
        R.id.cellGroupFragment3,
        R.id.cellGroupFragment4,
        R.id.cellGroupFragment5,
        R.id.cellGroupFragment6,
        R.id.cellGroupFragment7,
        R.id.cellGroupFragment8,
        R.id.cellGroupFragment9
    };

    for (int i = 1; i < 10; i++) {
      CellGroup thisCellGroup = (CellGroup) getSupportFragmentManager().findFragmentById(cellGroupFragments[i - 1]);
      if (thisCellGroup != null) {
        thisCellGroup.setGroupId(i);
      }
    }

    CellGroup tempCellGroup;
    for (int i = 0; i < 9; i++) {
      for (int j = 0; j < 9; j++) {
        int column = j / 3;
        int row = i / 3;

        int fragmentNumber = (row * 3) + column;
        tempCellGroup = (CellGroup) getSupportFragmentManager().findFragmentById(cellGroupFragments[fragmentNumber]);
        if (tempCellGroup != null) {
          int groupColumn = j % 3;
          int groupRow = i % 3;

          int groupPosition = (groupRow * 3) + groupColumn;
          int currentValue = mCurrentBoard.getValue(i, j);

          tempCellGroup.setValue(groupPosition, currentValue);
        }
      }
    }
  }

  private ArrayList<Board> readGameBoards(int difficulty, String pathFile) {
    ArrayList<Board> boards = new ArrayList<>();
    int fileId = PATTERN_EASY;

    if (difficulty == PATTERN_CLEAR) {
      Board board = new Board();
      for (int i = 0; i < 9; i++) {
        for (int j = 0; j < 9; j++) {
          board.setValue(i, j, 0);
        }
      }
      boards.add(board);
      return boards;
    } else if (difficulty == PATTERN_EASY) {
      fileId = R.raw.easy;
    } else if (difficulty == PATTERN_NORMAL) {
      fileId = R.raw.normal;
    } else if (difficulty == PATTERN_HARD) {
      fileId = R.raw.hard;
    } else {
      Log.d(TAG, "Another difficulty");
    }

    BufferedReader bufferedReader;
    try {
      if (pathFile != null) {
        FileInputStream fileIn = new FileInputStream(pathFile);
        bufferedReader = new BufferedReader(new InputStreamReader(fileIn));
      } else {
        InputStream inputStream = getResources().openRawResource(fileId);
        bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
      }

      String line = bufferedReader.readLine();
      if (line != null && line.startsWith("START")) {
        line = bufferedReader.readLine();
        while (line != null) {
          if (line.startsWith("END")) {
            bufferedReader.close();
            break;
          }

          Board board = new Board();
          // read all lines in the board
          for (int i = 0; i < 9; i++) {
            String[] rowCells = line.split(" ");
            for (int j = 0; j < 9; j++) {
              if (rowCells[j].equals("-")) {
                board.setValue(i, j, 0);
              } else {
                board.setValue(i, j, Integer.parseInt(rowCells[j]));
              }
            }
            line = bufferedReader.readLine();
          }
          boards.add(board);
          line = bufferedReader.readLine();
        }
        bufferedReader.close();
      } else {
        bufferedReader.close();
      }
    } catch (IOException e) {
      Log.e(TAG, e.getMessage());
    }

    return boards;
  }

  private Board chooseRandomBoard(ArrayList<Board> boards) {
    int randomNumber = (int) (Math.random() * boards.size());
    return boards.get(randomNumber);
  }

  private boolean isStartPiece(int group, int cell) {
    int row = ((group - 1) / 3) * 3 + (cell / 3);
    int column = ((group - 1) % 3) * 3 + ((cell) % 3);
    return mStartBoard.getValue(row, column) != 0;
  }

  private boolean checkAllGroups() {
    int[] cellGroupFragments = new int[] {
        R.id.cellGroupFragment,
        R.id.cellGroupFragment2,
        R.id.cellGroupFragment3,
        R.id.cellGroupFragment4,
        R.id.cellGroupFragment5,
        R.id.cellGroupFragment6,
        R.id.cellGroupFragment7,
        R.id.cellGroupFragment8,
        R.id.cellGroupFragment9
    };

    for (int i = 0; i < 9; i++) {
      CellGroup thisCellGroup = (CellGroup) getSupportFragmentManager().findFragmentById(cellGroupFragments[i]);
      if (thisCellGroup != null && !thisCellGroup.checkGroupCorrect()) {
        return false;
      }
    }
    return true;
  }

  public void trySolve() {
    if (!checkAllGroups()) {
      mTvStatus.setVisibility(View.VISIBLE);
      mTvStatus.setText(getString(R.string.board_cant_solved));
      // setToastView(this, getString(R.string.cant_solve));
      return;
    }

    int[][] matrix = {  { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                        { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                        { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                        { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                        { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                        { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                        { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                        { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                        { 0, 0, 0, 0, 0, 0, 0, 0, 0 } };

    int[] cellGroupFragments = new int[] {
        R.id.cellGroupFragment,
        R.id.cellGroupFragment2,
        R.id.cellGroupFragment3,
        R.id.cellGroupFragment4,
        R.id.cellGroupFragment5,
        R.id.cellGroupFragment6,
        R.id.cellGroupFragment7,
        R.id.cellGroupFragment8,
        R.id.cellGroupFragment9
    };

    int columnCtr = 0;
    int rowCtr = 0;
    for (int i = 0; i < 9; i++) {
      CellGroup thisCellGroup = (CellGroup) getSupportFragmentManager().findFragmentById(cellGroupFragments[i]);
      if (thisCellGroup != null) {
        List<Integer> numbers = thisCellGroup.getGroupNumbers();

        if (i != 0 && i % 3 == 0) {
          columnCtr = 0;
          rowCtr++;
        }

        int column = 0;
        int row = 0;
        for (Integer num : numbers) {
          if (column != 0 && column % 3 == 0) {
            row++;
            column = 0;
          }

          int newRow = row + (rowCtr * 3);
          int newColumn = column + (columnCtr * 3);
          matrix[newRow][newColumn] = num;

          column++;
        }

        columnCtr++;
      }
    }

    for (int i = 0; i < 9; i++) {
      StringBuilder dataRow = new StringBuilder();
      for (int j = 0; j < 9; j++) {
        dataRow.append("").append(matrix[i][j]);
      }
      Log.i(TAG, "row[" + i + "]:" +dataRow);
    }

    if (Solver.solve(0,0, matrix)) {
      mTvStatus.setText(getString(R.string.board_solved));
      columnCtr = 0;
      rowCtr = 0;

      for (int i = 0; i < 9; i++) {

        List<Integer> numbers = new ArrayList<>();

        if (i != 0 && i % 3 == 0) {
          columnCtr = 0;
          rowCtr++;
        }

        int column = 0;
        int row = 0;
        for (int j = 0; j < 9; j++) {
          if (column != 0 && column % 3 == 0) {
            row++;
            column = 0;
          }

          int newRow = row + (rowCtr * 3);
          int newColumn = column + (columnCtr * 3);

          int value = matrix[newRow][newColumn];
          numbers.add(value);
          mCurrentBoard.setValue(i, j, value);

          column++;
        }

        columnCtr++;

        CellGroup thisCellGroup = (CellGroup) getSupportFragmentManager().findFragmentById(cellGroupFragments[i]);
        if (thisCellGroup != null) {
          thisCellGroup.parseGroupSolving(numbers);
        }
      }
    } else {
      mTvStatus.setText(getString(R.string.board_cant_solved));
    }
    mTvStatus.setVisibility(View.VISIBLE);
  }

  public void checkBoard() {
    if (checkAllGroups() && mCurrentBoard.isBoardCorrect()) {
      mTvStatus.setText(getString(R.string.board_correct));
    } else {
      mTvStatus.setText(getString(R.string.board_incorrect));
    }
  }

  @Override
  public void onFragmentInteraction(int groupId, int cellId, View view) {
    mTvClickedCell = (TextView) view;
    mClickedGroup = groupId;
    mClickedCellId = cellId;

    Log.i(TAG, "Clicked group " + groupId + ", cell " + cellId);
    if (!isStartPiece(groupId, cellId)) {
      showBottomSheetInput();
    } else {
      Log.i(TAG, "NOT ALLOWED");
    }
  }

  public void showBottomSheetInput() {
    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View view = Objects.requireNonNull(inflater).inflate(R.layout.bottom_sheet_number_select, null);

    final Switch swUnsure = view.findViewById(R.id.sw_as_unsure);
    Button btnRemove = view.findViewById(R.id.btn_remove);

    final BottomSheetDialog dialog = new BottomSheetDialog(this);

    dialog.setContentView(view);
    dialog.show();

    View.OnClickListener listener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.dismiss();

        Log.d(TAG, "input at :" + v.getTag());
        final int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int input = numbers[Integer.parseInt((String) v.getTag())];
        int row = ((mClickedGroup - 1) / 3) * 3 + (mClickedCellId / 3);
        int column = ((mClickedGroup - 1) % 3) * 3 + ((mClickedCellId) % 3);

        mTvClickedCell.setText(String.valueOf(input));
        mCurrentBoard.setValue(row, column, input);

        boolean isUnsure = swUnsure.isChecked();
        if (isUnsure) {
          mTvClickedCell.setBackground(getResources().getDrawable(R.drawable.table_border_cell_unsure));
        } else {
          mTvClickedCell.setBackground(getResources().getDrawable(R.drawable.table_border_cell));
        }

        if (mCurrentBoard.isBoardFull()) {
          mTvStatus.setVisibility(View.VISIBLE);
          checkBoard();
        } else {
          mTvStatus.setVisibility(View.GONE);
        }
      }
    };

    int[] textViews = new int[] {
        R.id.textView1,
        R.id.textView2,
        R.id.textView3,
        R.id.textView4,
        R.id.textView5,
        R.id.textView6,
        R.id.textView7,
        R.id.textView8,
        R.id.textView9
    };

    for (int idTv : textViews) {
      TextView textView = view.findViewById(idTv);
      textView.setOnClickListener(listener);
    }

    btnRemove.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int row = ((mClickedGroup - 1) / 3) * 3 + (mClickedCellId / 3);
        int column = ((mClickedGroup - 1) % 3) * 3 + ((mClickedCellId) % 3);

        mTvClickedCell.setText("");
        mTvClickedCell.setBackground(getResources().getDrawable(R.drawable.table_border_cell));
        mCurrentBoard.setValue(row, column, 0);
        mTvStatus.setVisibility(View.GONE);

        dialog.dismiss();
      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    if (mHasReadStoragePermission) {
      getMenuInflater().inflate(R.menu.menu_pattern, menu);
    }
    return true;
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (resultCode == RESULT_OK) {
      if (requestCode == RC_FILE) {
        if (data != null) {
          String pathFile = FileUtils.getMediaPath(this, data.getData());
          Log.d(TAG, "FILE______ type, ori-path:" + pathFile);
          if (pathFile.endsWith(".pattern")) {
            prepareBoard(PATTERN_OTHER, pathFile);
          } else {
            setToastView(this, getString(R.string.cant_load_pattern));
          }
        }
      }
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;

      case R.id.pattern_clear:
        prepareBoard(PATTERN_CLEAR, null);
        return true;

      case R.id.pattern_1:
        prepareBoard(PATTERN_EASY, null);
        return true;

      case R.id.pattern_2:
        prepareBoard(PATTERN_NORMAL, null);
        return true;

      case R.id.pattern_3:
        prepareBoard(PATTERN_HARD, null);
        return true;

      case R.id.pattern_other:
        Log.d(TAG, "Other");
        try {
          startActivityForResult(createFileChooser(), RC_FILE);
        } catch (ActivityNotFoundException ex) {
          setToastView(this, getString(R.string.file_manager_check));
        }
        return true;

      default:
        return super.onOptionsItemSelected(item);
    }
  }

  protected Intent createFileChooser() {
    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
    intent.setType("*/*");
    intent.addCategory(Intent.CATEGORY_OPENABLE);

    // special intent for Samsung file manager
    Intent sIntent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
    // if you want any file type, you can skip next line
    sIntent.putExtra("CONTENT_TYPE", "plain/text");
    sIntent.addCategory(Intent.CATEGORY_DEFAULT);

    Intent chooserIntent;
    if (this.getPackageManager().resolveActivity(sIntent, 0) != null) {
      Log.d(TAG, "... Intent Samsung");
      // it is device with samsung file manager
      chooserIntent = Intent.createChooser(sIntent, getString(R.string.open_file));
      chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{intent});
    } else {
      Log.d(TAG, "... Intent Biasa");
      chooserIntent = Intent.createChooser(intent, getString(R.string.open_file));
    }

    return chooserIntent;
  }

  // ***.....Make toast center in toast field..... ***//
  public static void setToastView(Context context, String text) {
    if (context == null) return;

    Toast toast = Toast.makeText(context, "   " + text + "   ", Toast.LENGTH_SHORT);
    TextView v = toast.getView().findViewById(android.R.id.message);
    if (v != null) v.setGravity(Gravity.CENTER);
    toast.show();
  }
}
