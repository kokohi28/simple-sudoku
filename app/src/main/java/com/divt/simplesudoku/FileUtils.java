/* 
 * Copyright (C) 2007-2008 OpenIntents.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.divt.simplesudoku;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.MediaStore.Audio;
import android.provider.MediaStore.Video;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

/**
 * @version 2009-07-03
 * 
 * @author Peli
 *
 */
public class FileUtils {
  /** TAG for log messages. */
  static final String TAG = "FileUtils";

  /**
   * Whether the filename is a video file.
   * 
   * @param filename
   * @return
   */

  // public static boolean isVideo(String filename) {
  // String mimeType = getMimeType(filename);
  // if (mimeType != null && mimeType.startsWith("video/")) {
  // return true;
  // } else {
  // return false;
  // }
  // }

  /**
   * Whether the URI is a local one.
   * 
   * @param uri
   * @return
   */
  public static boolean isLocal(String uri) {
    if (uri != null && !uri.startsWith("http://")) {
      return true;
    }
    return false;
  }

	/**
	 * Gets the extension of a file name, like ".png" or ".jpg".
	 * 
	 * @param uri
	 * @return Extension including the dot("."); "" if there is no extension;
	 *         null if uri was null.
	 */
  public static String getExtension(String uri) {
    if (uri == null) {
      return null;
    }

    int dot = uri.lastIndexOf(".");
    if (dot >= 0) {
      return uri.substring(dot);
    } else {
      // No extension.
      return "";
    }
  }

	/**
	 * Returns true if uri is a media uri.
	 * 
	 * @param uri
	 * @return
	 */
	public static boolean isMediaUri(String uri) {
		if (uri.startsWith(Audio.Media.INTERNAL_CONTENT_URI.toString())
				|| uri.startsWith(Audio.Media.EXTERNAL_CONTENT_URI.toString())
				|| uri.startsWith(Video.Media.INTERNAL_CONTENT_URI.toString())
				|| uri.startsWith(Video.Media.EXTERNAL_CONTENT_URI.toString())) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Convert File into Uri.
	 * @param file
	 * @return uri
	 */
  public static Uri getUri(File file) {
    if (file != null) {
      return Uri.fromFile(file);
    }
    return null;
  }

	/**
	 * Convert Uri into File.
	 * @param uri
	 * @return file
	 */
  public static File getFile(Uri uri) {
    if (uri != null) {
      String filepath = uri.getPath();
      if (filepath != null) {
        return new File(filepath);
      }
    }
    return null;
  }
	
	/**
	 * Convert Uri string into File
	 * @param uristr
	 * @return file
	 */
  public static File getFile(String uristr) {
    if (uristr == null || uristr.isEmpty()) return null;
    File f = null;
    try{
      final URI uri = URI.create(uristr);
      f = new File(uri);
    } catch (Exception e){
      f = null;
    }
    return f;
  }
	
	/**
	 * Returns the path only (without file name).
	 * @param file
	 * @return
	 */
  public static File getPathWithoutFilename(File file) {
    if (file != null) {
      if (file.isDirectory()) {
        // no file to be split off. Return everything
        return file;
      } else {
        String filename = file.getName();
        String filepath = file.getAbsolutePath();

        // Construct path without file name.
        String pathwithoutname = filepath.substring(0, filepath.length() - filename.length());
        if (pathwithoutname.endsWith("/")) {
          pathwithoutname = pathwithoutname.substring(0, pathwithoutname.length() - 1);
        }
        return new File(pathwithoutname);
      }
    }
    return null;
  }

	/**
	 * Constructs a file from a path and file name.
	 * 
	 * @param curdir
	 * @param file
	 * @return
	 */
  public static File getFile(String curdir, String file) {
    String separator = "/";
    if (curdir.endsWith("/")) {
      separator = "";
    }
    File clickedFile = new File(curdir + separator + file);
    return clickedFile;
  }

  public static File getFile(File curdir, String file) {
    return getFile(curdir.getAbsolutePath(), file);
  }

  /* ******************************************************************** */

  // The original version:
  // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
  //

  /**
   * Get a file path from a Uri. This will get the the path for Storage Access
   * Framework Documents, as well as the _data field for the MediaStore and
   * other file-based ContentProviders.
   * 
   * @param context
   *          The context.
   * @param uri
   *          The Uri to query.
   * @author paulburke
   */
  public static String getPath(final Context context, final Uri uri) {
    // DocumentProvider
    if (hasKitKat() && DocumentsContract.isDocumentUri(context, uri)) {
      // ExternalStorageProvider
      if (isExternalStorageDocument(uri)) {
        final String docId = DocumentsContract.getDocumentId(uri);
        final String[] split = docId.split(":");
        final String type = split[0];

        if ("primary".equalsIgnoreCase(type)) {
          return Environment.getExternalStorageDirectory() + "/" + split[1];
        } else {
          if (new File("/storage/sdcard1/").exists()) {
            String sd1path = "/storage/sdcard1/";
            return sd1path + split[1];
          }
        }

        // TODO handle non-primary volumes
      }
      // DownloadsProvider
      else if (isDownloadsDocument(uri)) {

        final String id = DocumentsContract.getDocumentId(uri);
        final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

        return getDataColumn(context, contentUri, null, null);
      }
      // MediaProvider
      else if (isMediaDocument(uri)) {
        final String docId = DocumentsContract.getDocumentId(uri);
        final String[] split = docId.split(":");
        final String type = split[0];

        Uri contentUri = null;
        if ("image".equals(type)) {
          contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        } else if ("video".equals(type)) {
          contentUri = Video.Media.EXTERNAL_CONTENT_URI;
        } else if ("audio".equals(type)) {
          contentUri = Audio.Media.EXTERNAL_CONTENT_URI;
        }

        final String selection = "_id=?";
        final String[] selectionArgs = new String[]{split[1]};

        return getDataColumn(context, contentUri, selection, selectionArgs);
      }
    }
    // MediaStore (and general)
    else if ("content".equalsIgnoreCase(uri.getScheme())) {

      // Return the remote address
      if (isGooglePhotosUri(uri)) return uri.getLastPathSegment();

      return getDataColumn(context, uri, null, null);
    }
    // File
    else if ("file".equalsIgnoreCase(uri.getScheme())) {
      return uri.getPath();
    }

    return null;
  }

  /**
   * Get the value of the data column for this Uri. This is useful for
   * MediaStore Uris, and other file-based ContentProviders.
   * 
   * @param context
   *          The context.
   * @param uri
   *          The Uri to query.
   * @param selection
   *          (Optional) Filter used in the query.
   * @param selectionArgs
   *          (Optional) Selection arguments used in the query.
   * @return The value of the _data column, which is typically a file path.
   */
  public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
    Cursor cursor = null;
    final String column = "_data";
    final String[] projection = {column};
    try {
      cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
      if (cursor != null && cursor.moveToFirst()) {
        final int index = cursor.getColumnIndexOrThrow(column);
        return cursor.getString(index);
      }
    } catch (Exception e) {
      // jagad: pass through finally, then return null
    }

    if (cursor != null) cursor.close();
    cursor = null;

    return null;
  }

  /**
   * @param uri
   *          The Uri to check.
   * @return Whether the Uri authority is ExternalStorageProvider.
   */
  public static boolean isExternalStorageDocument(Uri uri) {
    // 06-04 10:22:18.073: D/ChatFragment(21612): pick file >>
    // content://com.android.externalstorage.documents/document/6771-1B15%3AEminem%20-%20Guts%20Over%20Fear%20ft.%20Sia.mp3
    // 06-04 10:24:13.803: D/ChatFragment(21612): pick file >>
    // content://com.android.externalstorage.documents/document/primary%3Aramdump%2Fmodem_crash_2014-07-24_23-25-02.log

    return "com.android.externalstorage.documents".equals(uri.getAuthority());
  }

  /**
   * @param uri
   *          The Uri to check.
   * @return Whether the Uri authority is DownloadsProvider.
   */
  public static boolean isDownloadsDocument(Uri uri) {
    return "com.android.providers.downloads.documents".equals(uri.getAuthority());
  }

  /**
   * @param uri
   *          The Uri to check.
   * @return Whether the Uri authority is MediaProvider.
   */
  public static boolean isMediaDocument(Uri uri) {
    return "com.android.providers.media.documents".equals(uri.getAuthority());
  }

  /**
   * @param uri
   *          The Uri to check.
   * @return Whether the Uri authority is Google Photos.
   */
  public static boolean isGooglePhotosUri(Uri uri) {
    return "com.google.android.apps.photos.content".equals(uri.getAuthority());
  }

  public static boolean copyFile(String inputFile, String outputPath, String fileName) {
    InputStream in = null;
    OutputStream out = null;
    try {
      // create output directory if it doesn't exist
      File dir = new File(outputPath);
      if (!dir.exists()) {
        dir.mkdirs();
      }

      in = new FileInputStream(inputFile);
      out = new FileOutputStream(outputPath + fileName);

      byte[] buffer = new byte[1024];
      int read;
      while ((read = in.read(buffer)) != -1) {
        out.write(buffer, 0, read);
      }
      in.close();
      in = null;

      // write the output file (You have now copied the file)
      out.flush();
      out.close();
      out = null;

    } catch (FileNotFoundException e) {
      Log.e(TAG, e.getMessage());
      return false;
    } catch (Exception e) {
      Log.e(TAG, e.getMessage());
      return false;
    }

    return true;
  }

  public static void CopyStream(InputStream is, OutputStream os) {
    final int buffer_size = 1024;
    try {
      byte[] bytes = new byte[buffer_size];
      for (;;) {
        int count = is.read(bytes, 0, buffer_size);
        if (count == -1) break;
        os.write(bytes, 0, count);
      }
    } catch (Exception ex) {
    }
  }

  private static boolean hasKitKat() {
    /* Android 4.4 API Level 19 */
    return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
  }

  public static String getMediaPath(final Activity activityContext, Uri uri) {
    String fixPath = null;

    // jagad: try using file util first...
    String path = FileUtils.getPath(activityContext, uri);
    if (path != null) {
      fixPath = path;
    } else {
      // jagad: google photos trouble maker fixing.
      // reference: http://stackoverflow.com/questions/30527045/choosing-photo-using-new-google-photos-app-is-broken
      String contUri = uri.getPath();
      int startIndex = contUri.indexOf("external/");
      int endIndex = contUri.indexOf("/ACTUAL");
      if (startIndex != -1 && endIndex != -1) {
        String embeddedPath = contUri.substring(startIndex, endIndex);
        Uri.Builder builder = uri.buildUpon();
        builder.path(embeddedPath);
        builder.authority("media");
        Uri newUri = builder.build();

        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = activityContext.managedQuery(newUri, projection, null, null, null);
        if (cursor != null) {
          int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
          cursor.moveToFirst();
          String flpath = cursor.getString(column_index);
          if (flpath != null) fixPath = flpath;
        }

        if (cursor != null) cursor.close();
        cursor = null;
      }

      // jagad: we just return the path from uri.
      if (fixPath == null) fixPath = uri.getPath();
    }

    // Koko, Some Workaround
    // This got from external SD CARD LG-G5
    // /document/0B09-1317:_audio/August Rush/August Rush - Bach Break.mp3
    if (fixPath.contains(":")) {
      String[] split = fixPath.split(":");
      String[] devStr = split[0].split("/");
      fixPath = "/storage/" + devStr[devStr.length - 1] + "/" + split[1];

      // Log.d(TAG, "...path:" + split + " " + devStr + " " + fixPath);
    }

    Log.d(TAG, "...fixPath:" + fixPath);
    return fixPath;
  }
}
