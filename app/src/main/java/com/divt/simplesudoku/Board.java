package com.divt.simplesudoku;

import java.util.ArrayList;

public class Board {
  private int[][] mCells = new int[9][9];

  Board() {
  }

  void setValue(int row, int column, int value) {
    mCells[row][column] = value;
  }

  int[][] getCells() {
    return mCells;
  }

  void copyValues(int[][] newGameCells) {
    for (int i = 0; i < newGameCells.length; i++) {
      if (newGameCells[i].length >= 0)
        System.arraycopy(newGameCells[i], 0, mCells[i], 0, newGameCells[i].length);
    }
  }

  boolean isBoardFull() {
    for (int[] mCell : mCells) {
      for (int i : mCell) {
        if (i == 0) {
          return false;
        }
      }
    }
    return true;
  }

  boolean isBoardCorrect() {
    // Check horizontal
    for (int[] mCell : mCells) {
      ArrayList<Integer> numbers = new ArrayList<>();
      for (int number : mCell) {
        if (numbers.contains(number)) {
          return false;
        } else {
          numbers.add(number);
        }
      }
    }

    // Check vertical
    for (int i = 0; i < mCells.length; i++) {
      ArrayList<Integer> numbers = new ArrayList<>();
      for (int j = 0; j < mCells[i].length; j++) {
        int number = mCells[j][i];
        if (numbers.contains(number)) {
          return false;
        } else {
          numbers.add(number);
        }
      }
    }

    return true;
  }

  int getValue(int row, int column) {
    return mCells[row][column];
  }

  @Override
  public String toString() {
    StringBuilder temp = new StringBuilder();
    for (int[] mCell : mCells) {
      for (int j = 0; j < mCell.length; j++) {
        if (j == 0) {
          temp.append("\n");
        }

        int currentNumber = mCell[j];
        if (currentNumber == 0) {
          temp.append("-");
        } else {
          temp.append(currentNumber);
        }

        if (j != (mCell.length - 1)) {
          temp.append(" ");
        }
      }
    }
    return temp.toString();
  }
}
