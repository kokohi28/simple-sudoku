package com.divt.simplesudoku;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CellGroup extends Fragment {
  private int mGroupId;
  private OnFragmentInteractionListener mListener;
  private View mView;

  public CellGroup() {
    // Required empty public constructor
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    mView = inflater.inflate(R.layout.cell_group, container, false);

    int[] textViews = new int[] {
        R.id.textView1,
        R.id.textView2,
        R.id.textView3,
        R.id.textView4,
        R.id.textView5,
        R.id.textView6,
        R.id.textView7,
        R.id.textView8,
        R.id.textView9
    };

    for (int idTv : textViews) {
      TextView textView = mView.findViewById(idTv);
      textView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          mListener.onFragmentInteraction(mGroupId, Integer.parseInt(view.getTag().toString()), view);
        }
      });
    }
    return mView;
  }

  public void setGroupId(int mGroupId) {
    this.mGroupId = mGroupId;
  }

  public void setValue(int position, int value) {
    int[] textViews = new int[] {
        R.id.textView1,
        R.id.textView2,
        R.id.textView3,
        R.id.textView4,
        R.id.textView5,
        R.id.textView6,
        R.id.textView7,
        R.id.textView8,
        R.id.textView9
    };

    TextView currentView = mView.findViewById(textViews[position]);
    if (value == 0) {
      currentView.setText("");
    } else {
      currentView.setText(String.valueOf(value));
      currentView.setTextColor(Color.parseColor("#009688"));
      currentView.setTypeface(null, Typeface.BOLD);
    }
  }

  public boolean checkGroupCorrect() {
    List<Integer> numbers = new ArrayList<>();

    int[] textViews = new int[] {
        R.id.textView1,
        R.id.textView2,
        R.id.textView3,
        R.id.textView4,
        R.id.textView5,
        R.id.textView6,
        R.id.textView7,
        R.id.textView8,
        R.id.textView9
    };

    for (int idTv : textViews) {
      TextView textView = mView.findViewById(idTv);
      String numStr = textView.getText().toString();
      if (numStr.isEmpty()) numStr = "0";
      int number = Integer.parseInt(numStr);
      if (number != 0) {
        if (numbers.contains(number)) {
          return false;
        } else {
          numbers.add(number);
        }
      }
    }
    return true;
  }

  public List<Integer> getGroupNumbers() {
    List<Integer> numbers = new ArrayList<>();

    int[] textViews = new int[] {
        R.id.textView1,
        R.id.textView2,
        R.id.textView3,
        R.id.textView4,
        R.id.textView5,
        R.id.textView6,
        R.id.textView7,
        R.id.textView8,
        R.id.textView9
    };

    for (int idTv : textViews) {
      TextView textView = mView.findViewById(idTv);
      String numStr = textView.getText().toString();
      if (numStr.isEmpty()) numStr = "0";
      numbers.add(Integer.parseInt(numStr));
    }

    return numbers;
  }

  public void parseGroupSolving(List<Integer> numbers) {
    int[] textViews = new int[] {
        R.id.textView1,
        R.id.textView2,
        R.id.textView3,
        R.id.textView4,
        R.id.textView5,
        R.id.textView6,
        R.id.textView7,
        R.id.textView8,
        R.id.textView9
    };

    int i = 0;
    for (int idTv : textViews) {
      TextView textView = mView.findViewById(idTv);
      String numStr = textView.getText().toString();
      if (numStr.isEmpty()) {
        textView.setText(String.valueOf(numbers.get(i)));
        textView.setTextColor(Color.parseColor("#3F51B5"));
        // textView.setTypeface(null, Typeface.BOLD);
      }
      i++;
    }
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      mListener = (OnFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  public interface OnFragmentInteractionListener {
    void onFragmentInteraction(int groupId, int cellId, View view);
  }
}
