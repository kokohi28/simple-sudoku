package com.divt.simplesudoku;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

public class MPermsManager {
  private final static String TAG = MPermsManager.class.getSimpleName();

  public final static int REQUEST_ALL_MANDATORY = 100;
  public final static int REQUEST_ALL_OPTIONAL = 101;
  public final static int REQUEST_SINGLE_PERMISSION = 102;
  public final static int REQUEST_WITH_SPECIFIC_PURPOSE = 103;

  // private boolean mAllPermsGranted = false;
  // private boolean mHasPendingRequestPermission = false;

  // private static MPermsManager msInstance = null;
  //
  // public static MPermsManager getInst() {
  // if (msInstance == null) {
  // msInstance = new MPermsManager();
  // }
  // return msInstance;
  // }
  //
  // public static void destroyInst() {
  // msInstance = null;
  // }

  // @AndroidManifest
  // <uses-permission android:name="android.permission.READ_CONTACTS" />
  // <uses-permission android:name="android.permission.INTERNET" />
  // <uses-permission android:name="android.permission.WAKE_LOCK" />
  // <uses-permission android:name="android.permission.VIBRATE" />
  // <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
  // <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
  // <uses-permission android:name="android.permission.RECORD_AUDIO" />
  // <uses-permission android:name="android.permission.READ_PHONE_STATE" />
  // <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
  // <uses-permission android:name="android.permission.CAMERA" />
  // <uses-permission android:name="android.permission.WRITE_SETTINGS" />
  // <uses-permission android:name="android.permission.WRITE_INTERNAL_STORAGE" />
  // <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
  // <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
  // <uses-permission android:name="android.permission.MOUNT_UNMOUNT_FILESYSTEMS" />
  // <uses-permission android:name="android.permission.MODIFY_AUDIO_SETTINGS" />
  // <uses-permission android:name="android.permission.CAPTURE_AUDIO_OUTPUT" />
  // <uses-permission android:name="android.permission.SET_TIME_ZONE" />
  // <uses-permission android:name="com.android.launcher.permission.INSTALL_SHORTCUT" />
  // <uses-permission android:name="com.android.launcher.permission.UNINSTALL_SHORTCUT" />
  // <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
  // <uses-permission android:name="android.permission.SEND_SMS" />
  // <uses-permission android:name="android.permission.RECEIVE_SMS" />
  // <!-- <uses-permission android:name="com.android.vending.BILLING" /> -->
  // <uses-permission android:name="android.permission.READ_PHONE_STATE" />
  // <uses-permission android:name="android.permission.CALL_PHONE" />

  public final static String P_READ_PHONE_STATE = Manifest.permission.READ_PHONE_STATE; // [v] used for Read IMEI, Network Information, etc.

  @SuppressLint("InlinedApi")
  // @Jelly bean
  public final static String P_READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE; // mandatory needs
  public final static String P_WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE; // mandatory needs
  public final static String P_CAMERA = Manifest.permission.CAMERA; // [v] used for take photo or video
  public final static String P_RECORD_AUDIO = Manifest.permission.RECORD_AUDIO; // [v] used for using microphone
  public final static String P_CONTACTS = Manifest.permission.READ_CONTACTS; // [v] used for Sync Contact
  public final static String P_SEND_SMS = Manifest.permission.SEND_SMS; // [v] used for login auto detect code [opt]
  public final static String P_RECEIVE_SMS = Manifest.permission.RECEIVE_SMS; // [v] used for login auto detect code
  public final static String P_READ_SMS = Manifest.permission.READ_SMS; // [v] used for login auto detect code

  // public boolean getAllPermsState() {
  // return mAllPermsGranted;
  // }
  //
  // public boolean hasPendingReq() {
  // return mHasPendingRequestPermission;
  // }

  public static boolean isPermGranted(Activity activity, String perm) {
    if (hasM()) {
      if (ActivityCompat.checkSelfPermission(activity, perm) == PackageManager.PERMISSION_GRANTED) {
        Log.d(TAG, " --- isPermsGranted: " + perm);
        return true;
      }
      return false;
    } else {
      Log.d(TAG, " --- UNDER M isPermsGranted: " + perm);
      return true;
    }
  }

  public static boolean isAllPermsGranted(Context context, String[] perms) {
    boolean hasNotPermit = false;

    if (hasM()) {
      for (int i = 0; i < perms.length; i++) {
        if (ActivityCompat.checkSelfPermission(context, perms[i]) != PackageManager.PERMISSION_GRANTED) {
          Log.d(TAG, " --- isPermGranted[" + i + "]:" + perms[i]);
          hasNotPermit = true;
        }
      }
    } else {
      Log.d(TAG, " --- UNDER M all isPermsGranted");
    }

    Log.d(TAG, " --- isAllPermsGranted:" + !hasNotPermit);

    return !hasNotPermit;
  }

  public static boolean verifyPermissions(int[] grantResults) {
    // At least one result must be checked.
    if (grantResults.length < 1) {
      return false;
    }

    // Verify that each required permission has been granted, otherwise return false.
    for (int result : grantResults) {
      if (result != PackageManager.PERMISSION_GRANTED) {
        return false;
      }
    }
    return true;
  }

  public static boolean verifyWithSpecificPermission(int[] grantResults, String[] permissions, String specificPerm) {
    // At least one result must be checked.
    if (grantResults.length < 1 || permissions.length < 1) {
      return false;
    }

    Log.d(TAG, " --- verifyWithSpecificPermission pre:" + grantResults.length + " " + permissions.length);

    // Ambil yg kecil (JIKA tidak sama)
    int n = ((grantResults.length < permissions.length) ? grantResults.length : permissions.length);

    // Verify that each required permission has been granted, otherwise return false.
    for (int i = 0; i < n; i++) {
      int result = grantResults[i];
      String perm = permissions[i];

      Log.d(TAG, " --- Permissions data:" + result + " " + perm);
      if (perm.equals(specificPerm) && result == PackageManager.PERMISSION_GRANTED) {
        return true;
      }
    }

    return false;
  }

  public static boolean verifyWithSpecificPermissions(int[] grantResults, String[] permissions, String[] specificPerms) {
    // At least one result must be checked.
    if (grantResults.length < 1 || permissions.length < 1) {
      return false;
    }

    Log.d(TAG, " --- verifyWithSpecificPermissions pre:" + grantResults.length + " " + permissions.length + " " + specificPerms.length);

    boolean success = true; // default all granted,
    // Verify that each required permission has been granted, otherwise return false.
    for (int i = 0; i < specificPerms.length; i++) {
      String reqPerm = specificPerms[i];
      boolean reqPerm_pass = true;
      for (int j = 0; i < permissions.length; j++) {
        String perm = permissions[j];
        if (reqPerm.equals(perm)) {
          Log.d(TAG, " --- perm :" + reqPerm + " " + grantResults[j]);
          if (grantResults[j] == PackageManager.PERMISSION_GRANTED) {
            reqPerm_pass = true;
          } else {
            reqPerm_pass = false;
          }
          break;
        }
      }

      if (!reqPerm_pass) {
        success = false;
        Log.d(TAG, " --- failed @perm :" + reqPerm);
        break;
      }
    }

    return success;
  }

  @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
  public static void requestPerm(Activity activity, String perm, int reqCode) {
    String[] perms = new String[]{perm};
    requestPerms(activity, perms, reqCode);
  }

  @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
  public static void requestPerms(Activity activity, String[] perms, int reqCode) {
    if (hasM()) {
      ActivityCompat.requestPermissions(activity, perms, reqCode);
    } else {
      Log.d(TAG, " --- UNDER M no need requestPerms");
    }
  }

  private static boolean hasM() {
    return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
  }
}
